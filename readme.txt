PASOS PARA INTEGRAR BLOG A PROYECTO LARAVEL EXISTENTE

NO TENGO CRUDBOOSTER INSTALADO
	Sigue los pasos descritos en el siguiente enlace https://github.com/crocodic-studio/crudbooster/blob/master/docs/en/installation.md

YA TENGO CRUDBOOSTER INSTALADO

		1. Agrega lo siguiente al archivo composer.json

			"repositories":[
				{
					"type":"vcs",
					"url":"https://bitbucket.org/3pixelsmedia/blog-package.git"
				}
			]

		2. Ejecuta por consola: composer require 3pixelsmedia/blog-package:dev-master


		3. Agrega la siguiente clase al array "providers" en el archivo config / app.php
	
			Trespixelsmedia\Blog\BlogServiceProvider::class,
		

		4. Ejecuta los siguientes comando desde el terminal en el directorio raíz del proyecto:
			composer dump-autoload
			php artisan vendor:publish --tag=blog
			
			Opcional (Recomendado para cargar los assets base):
			php artisan vendor:publish --tag=blogtheme

			Opcional (Para mostrar las imágenes almacenadas en el storage en el administrador):
			php artisan vendor:publish --tag=crudbooster-storage

		5. Ejecuta las migraciones

		6. Ejecuta el siguiente comando
			composer dump-autoload

		7. Ejecuta los seeders ubicados en el directorio seeds/blog
			Obligatorio:
				BlogModuleSeeder
			Opcional (post y categorias precargadas):
				CategoriesTableSeeder
         		DefaultPostsSeeder
				

		8. Agrega las siguientes rutas al archivo de rutas web del proyecto:
		//**** Blog (Public Routes) *****************************************************

		//Blog Routes Blog Home
		Route::get('blog','FrontBlogController@getIndex');

		//Blog Routes Search
		Route::get('search','FrontBlogController@getSearch');

		//Blog Route Latest
		Route::get('latest','FrontBlogController@getLatest');

		//Blog Route Category
		Route::get('blog/categorias/{slug}','FrontBlogController@getCategory');

		//Blog Route Article
		Route::get('blog/{slug}','FrontBlogController@getArticle');
		
		//Get more articles (Blog)
		Route::post('/blog-get-more-posts','FrontBlogController@getMorePosts');

		//Suscripcion
		Route::post('subscribe','SubscribeController@save')->name('subscribe-form');

		//*****************************************************************************

		9. Para activar los widgets es necesario agregar al composer.json:
			"autoload": {
				"files": [
					"app/helpers.php"
				]
			}

		9.1. Ejecuta el comando composer update

		10. El paquete trae integrado un botón de suscripción por email usando Mailchimp.
			Si Mailchimp no esta instalado deberás hacerlo siguiendo los pasos a continuación o 
			visitar la documentación: https://packagist.org/packages/skovmand/mailchimp-laravel

			10.1. Ejecutar por consola: composer require skovmand/mailchimp-laravel
			10.2. Registrar el provider en config/app.php
				'providers' => [
					Skovmand\Mailchimp\MailchimpServiceProvider::class,
					]
			10.3. Ejecutar por consola: php artisan vendor:publish --provider="Skovmand\Mailchimp\MailchimpServiceProvider"
			10.4. Agregar en el .env el API key: MAILCHIMP_API_KEY="your-api-key-here"

		11. Las imágenes en este paquete funcionan almacenandolas en un bucket de amazon s3 por tanto debe
			ser configurado, para esto:

			11.1. En el archivo config/filesystem.php configura el default de la siguiente forma:
					'default' => env('FILESYSTEM_DRIVER', 's3'),

			11.2. Agrega las credenciales del bucket en el .env, ejemplo:
					AWS_KEY=XXXXXXXXXXXXXX
					AWS_SECRET=XXXXXXXXXXXXXXXX
					AWS_REGION=us-east-1
					AWS_BUCKET=nombre-del-bucket	

			11.2. Asegúrate de tener instalado en el composer.json el "league/flysystem-aws-s3-v3". 
					si no lo tienes entonces escribe en la consola: composer require league/flysystem-aws-s3-v3	
			
			11.3. si Quieres visualizar las imágenes placeholder de los post de prueba y checar que funciona puedes
					copiar la carpeta "images" que se encuentra dentro de assets/blogtheme/ a tu bucket de amazon s3 
		