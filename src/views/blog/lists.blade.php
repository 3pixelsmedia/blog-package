@extends('blog.layout')

    @section('title', $page_title)

@section('primary-content')
    @include('blog.partials.menu-blog')

    @include('blog.partials.carousel')

@endsection

@section('content')
    <div class="separator"></div>
    @php 
        $locale = App::getLocale();
    @endphp 

    {!! getBlogWidgets('LIST', 'top-section') !!}

    {!! getBlogWidgets('LIST', 'before-content') !!}

 
        <div class="row">
            <div class="col-md-10 list-title">
                <span>Se han encontrado {{ count($result) }} resultados para: {{ $word_search }}</span>
            </div>
        </div>

    
    @foreach($result as $row)
        <!-- Blog Post -->

        <article class="row">
            <div class="col-md-4 card-post">
                <!-- Title -->
                
                <p class="img-post-list"><img src='{{ !empty($row->preview_image)? Storage::url($row->preview_image) : NULL }}' style="max-width: 100%"/></p>
                
            </div> 
            <div class="col-md-8"> 
                <div>
                    <span class="cat-span"><a href='{{URL::to("/")}}/blog/categorias/{{$row->slug_category }}'>{{ $row->name_categories }}</a></span>
                </div>

                <h2 class="post-title"> <a class="primary-color-text" href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>{{ $row->title }}</a></h2>
                    <!-- Author -->
                <p class="lead-blog">
                Publicado por: {{ $row->name_author }} <span style="display: block;">{{ date('M, d Y',strtotime($row->created_at)) }}</span> 
                </p>
                <!-- Date/Time -->
                <p class="date-blog"></p>
                <!-- Post Content -->
                <p class="post-resume">{{ $row->summary }} <span class="read-more"><a href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>Leer Más</a></span></p>
            </div>
        </article>
        <!-- /Blog Post --> 
    @endforeach


    @if(count($result)==0)
        <div class='alert alert-info'>No existen resultados para esta busqueda</div>
    @endif

    {!! getBlogWidgets('List', 'after-content') !!}

    {!! getBlogWidgets('List', 'bottom-section') !!}   

    <div class="col-xs-12 text-center" style="margin-top: 5%; margin-bottom: 5%;">
        <a class="btn btn-primary btn-form-send btn-form-send-blue" style="text-align: center !important; float:none;" href='{{URL::to("/")}}/blog/'>Regresar</a>
    </div>     

@endsection