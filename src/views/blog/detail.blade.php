@extends('blog.layout')

@section('title', $page_title)

@section('primary-content')
    @include('blog.partials.menu-blog')

    @include('blog.partials.carousel')

@endsection

@section('content')


    {!! getBlogWidgets('Detail','top-section', $row) !!}

            <article class="col-xs-12 col-sm-11 post-inner-container" style="float:left">

                
                    {!! getBlogWidgets('Detail','before-content', $row) !!}
                    
                    {!! $row->content !!}

                    {!! getBlogWidgets('Detail', 'after-content', $row) !!}

                    <div class="content" style="min-height:0px;">            
                        <div class="row">
                            @foreach($posts as $post)
                                <!-- Blog Post -->
                                <div class="col-md-4 card-post">
                                <!-- Title -->
                                    <p class="img-post-list">
                                        <a href='{{URL::to("/")}}/blog/{{ $post->slug }}' title='{{$post->title}}'>
                                            <img src="{{ !empty($post->preview_image)? Storage::url($post->preview_image) : NULL }}" style="max-width: 100%"/>
                                        </a>
                                    </p>
                                    <span class="cat-span"><a href='{{URL::to("/")}}/blog/categorias/{{$post->slug_category }}'>{{ $post->name_categories }}</a></span>
                                    <h2 class="post-title"> <a class="primary-color-text" href='{{URL::to("/")}}/blog/{{ $post->slug }}' title='{{$post->title}}'>{{ $post->title }}</a></h2>    
                                </div>
                                <!-- /Blog Post -->          
                            @endforeach     
                        </div>
                    </div>
                    {!! getBlogWidgets('Detail','bottom-section', $row) !!}
            </article>
            <div class="col-sm-1 hidden-xs social-container" style="float:left" >
            <!-- Blog Post -->
            {!! getBlogWidgets('Detail','social-section', $row) !!}<!-- 'Home','all', 'top-section' -->
                
            </div>


    

    <script>
        $(document).ready(function(){
           var h = $('.post-inner-container').css( "height" );
           $('.social-container').css({ "height": h });
           var s = $('.social-container').css( "height" );
           
           var url = window.location.href;
           $('#fb-sharer').attr("href", "https://www.facebook.com/sharer/sharer.php?u="+url+"");
           $('#tw-sharer').attr("href", "https://twitter.com/share?&url="+url);
           $('#ln-sharer').attr("href", "http://www.linkedin.com/shareArticle?url="+url);
           });

    </script>
@endsection
