@foreach ($widgets as $widget) 
	@if(!empty($widget))
		<div class="clear"></div>
		{!! $widget['html'] !!}
		<div class="clear"></div>
	@endif
@endforeach
