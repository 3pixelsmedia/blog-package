<div> 
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        
        @if($interface == 'blog-home')

        <div class="carousel-inner" role="listbox" id="carousel-inner">
            <div class="item active" style="background: url('{{ !empty($result[0]->main_image)? Storage::url($result[0]->main_image) :  asset('blogtheme/images/placeholder_main_img.png') }}')center center / cover;">
                <div class="overlay-banner-blog"></div>    
                @if(!empty($result[0])) 
                    <div class="carousel-caption caption-blog ">
                        <div class="col-sm-6 col-md-offset-6  offset-md-6 col-sm-offset-6 offset-sm-6">
                            <a href='{{URL::to("/")}}/blog/{{ $result[0]->slug }}'>
                                <h1 class="subtitle main-p1 carousel-caption-p caption-p-blog primary-color-text">{{ $result[0]->title }}</h1>
                            </a> 
                             <p class="lead-blog">
                              {{ $result[0]->name_author }}
                            </p>
                            <p class="post-resume">{{ str_limit($result[0]->summary,120) }}<p>
                            <span class="read-more"><a href='{{URL::to("/")}}/blog/{{$result[0]->slug }}' title='{{$result[0]->title}}'>Leer Más</a></span>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @elseif($interface == 'article')
            @if(!empty($row))
                <div class="carousel-inner" role="listbox" id="carousel-inner">
                    <div class="item active" style="background: url('{{ !empty($row->main_image)? Storage::url($row->main_image) :  asset('blogtheme/images/placeholder_main_img.png') }}')center center / cover;">
                    
                    <div class="overlay-banner-blog"></div>   
                    <div class="carousel-caption caption-blog "><!--m-b-5-->
                            <div class="col-sm-6 col-md-offset-6 offset-md-6 col-sm-offset-6 offset-sm-6">
                                <h1 class="subtitle main-p1 carousel-caption-p caption-p-blog primary-color-text">{{ $row->title }}</h1>
                                <p class="lead ">
                                    Por {{ $row->name_author }}
                                </p>

                                <p><span class="glyphicon glyphicon-time"></span> Publicado el {{ date('M,d Y',strtotime($row->created_at)) }}</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="carousel-inner" role="listbox" id="carousel-inner">
                    <div class="item active" style="background: url('{{ asset('blogtheme/images/placeholder_main_img.png') }}')center center / cover;">
                        <div class="overlay-banner-blog"></div>
                    </div>
                </div> 
            @endif
        @elseif($interface == 'category')

        <div class="carousel-inner blog-list-carousel" role="listbox" id="carousel-inner">
            <div class="item active" style="background: url('{{ asset('blogtheme/images/placeholder_main_img.png') }}')center center / cover;">
            <div class="overlay-banner-blog"></div>   
                <div class="carousel-caption caption-blog  m-b-5">
                        <div class="col-sm-12 full-div no-bg" style="margin-bottom:65px;">
                                <h1 class="primary-color-text">{{ $header_title }}</h1>

                        </div>
                </div>
            </div>
        </div>
        
        @elseif($interface == 'blog-search')
        <div class="carousel-inner blog-list-carousel" role="listbox" id="carousel-inner">
           
            <div class="item active" style="background: url('{{ asset('blogtheme/images/placeholder_main_img.png') }}')center center / cover;">
            <div class="overlay-banner-blog"></div>
            </div>
        </div>
 
        @endif                  
        </div>
    </div>
</div>
<div class="clear"></div>
@if($interface == 'blog-home')
<div class='col-md-12 no-gutter image-head menu-active' style="height: 250px;background:url('{{ !empty($result[0]->main_image)? Storage::url($result[0]->main_image) :  asset('blogtheme/images/placeholder_main_img.png') }}')center left / cover;">
    <div class="overlay-banner-blog" style="min-height: 210px;"></div> 
   <!--  <img class='img-responsive' src="{{ asset('images/blog_banner.jpg') }}" alt=""> -->
</div>
<div class='clearfix'></div>
<div class='col-md-12 content-head content-head-blog menu-active'>

</div>
@elseif($interface == 'article')
<div class='col-md-12 no-gutter image-head menu-active' style="height: 250px;background:url({{ !empty($row->main_image)? Storage::url($row->main_image) :  asset('blogtheme/images/placeholder_main_img.png') }})center left / cover;">
    <div class="overlay-banner-blog" style="min-height: 210px;"></div> 
   <!--  <img class='img-responsive' src="{{ asset('images/blog_banner.jpg') }}" alt=""> -->
</div>
<div class='clearfix'></div>
<div class='col-md-12 content-head content-head-blog menu-active'>
<h1 class="text-center primary-color-text bold-text">{{ $row->title }}</h1>
    <!-- <p class=" main-p1 carousel-caption-p rose-text font-weight-normal">{{ __('blog.posted') }} {{ date('M,d Y',strtotime($row->created_at)) }}</p> -->
</div>
@elseif($interface == 'category')
<div class='col-md-12 no-gutter image-head menu-active' style="height: 200px;background:url({{ asset('blogtheme/images/placeholder_main_img.png') }})center left / cover;">
    
</div>
<div class='clearfix'></div>
<div class='col-md-12 content-head content-head-blog menu-active' style="margin-bottom:30px;">
<span class="carousel-caption-span primary-color-text bold-text" style="font-size:18px">Categoría</span>
    <p class="subtitle main-p1 carousel-caption-p primary-color-text font-weight-normal">{{ $header_title }}</p>
</div>
@elseif($interface == 'blog-search')
<div class='col-md-12 no-gutter image-head menu-active'>

    <img class='img-responsive' src="{{ asset('blogtheme/images/placeholder_main_img.png') }}" alt="">
</div>
<div class='clearfix'></div>
<div class='col-md-12 content-head content-head-blog menu-active'>
<!-- <span class="carousel-caption-span rose-text bold-text">{{ __('blog.search_title') }}</span>
    <p class="subtitle main-p1 carousel-caption-p  rose-text font-weight-normal">{{ $header_title }}</p> -->
</div>
@endif
<div class="clear"></div>


