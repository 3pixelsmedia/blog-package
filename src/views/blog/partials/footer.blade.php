<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-3">
      <h3 style="margin-top:0">About Us</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
         Pellentesque imperdiet consectetur dolor in elementum.</p>

      </div>
      <div class="col-md-2 list">
        <ul>
          <li><a href="#">Lorem Ipsum</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
        </ul>

      </div>
      <div class="col-md-2 list">
        <ul>
          <li><a href="#">Lorem Ipsum</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
          <li><a href="#">Lorem Ipsum</a></li>
        </ul>

      </div>
      <div class="col-md-2">
    
      </div>
      <div class="col-md">
        <h4><strong>Contact Info</strong></h4>
        <p><strong>Adress:</strong> 514 S. Magnolia St.
Orlando, FL 32806 <!-- Random Adress -->
</p>
        <p><strong>Email:</strong> somebody@loremipsum.com</p>
        <p><strong>Tel.:</strong> (888) 888 8888</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md text-center">

      © Copyright <?php echo date("Y");?>

    </div> 

    

  
</footer>