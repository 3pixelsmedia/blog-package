<div class="modal fade" id="modal-subscribe" tabindex="-1" role="dialog" aria-labelledby="modal-subscribe-label" aria-hidden="true">
  <div class="modal-dialog cont" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
           <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="row">
        <div class="modal-body modal-b-subscribe">
            
            <div class="col-xs-10 col-xs-offset-1 offset-xs-1  ml-auto text-center text-modal-content ">
                <h4 class="blue-dark-text" >Suscríbete a nuestro blog y obtén actualizaciones de temas novedosos en tu email cada semana.</h4>  
            </div>
            @if($errors->any())
                <div class="alert alert-danger text-left">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
            @endif
            <form action="{{ route('subscribe-form') }}" id="subscription-form" method="POST" class="text-left">
                <input id="_token" name="_token" hidden value="{!! csrf_token() !!}" />
                <div class="col-xs-8 col-xs-offset-2 offset-xs-2 col-sm-6 col-sm-offset-3 offset-sm-3 input-container">
                    <!-- <label for="email" class="input-label">{{ __('plan.contact_email_label') }}</label> -->
                    <div class="@if($errors->get('semail')) has-error @endif">
                        <input type="email" name="semail" id="semail" class="form-control underline-input" style="width:100%" placeholder="Email" value="{{ old('semail') }}">
                    </div>
                    <div class="alert alert-success-modal-pre alert-dismissible blue-dark-text fade text-center" role="alert">
                     Enviando...
                    </div>
                    <div class="alert alert-success-modal alert-dismissible blue-dark-text fade text-center" role="alert">
                    ¡Gracias por Suscribirse!
                    </div>
                    <div class="alert alert-error alert-dismissible fade text-center" role="alert">
                     Escriba un Email válido 
                    </div>
                </div>

              </form>
        </div>
      </div>
      <div class="row">
        <div class="modal-footer footer-modal-subscribe">
            <button type="submit" id="subscription-submit" class="btn-subscription-modal">Suscribirse</button>
        </div>
      </div>

    </div>
  </div>
</div>

<style type="text/css">
.alert-success-modal, .alert-success-modal-pre {
    background-color: transparent;
    border-color: transparent; 
    /*color: #000000;*/
    font-weight:700;
    display: none;
    text-align:center;
    margin-bottom:0px;
    padding-bottom:0px;
    padding:0px;
    padding-top:20px;
    font-size:16px;
}
.alert-error{
  color:red;
  font-weight:700;
  display: none;
  margin-bottom:0px;
  padding-bottom:0px;
  padding:0px;
  padding-top:20px;
  font-size:16px;
}
</style>

<script type="text/javascript">
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
    }

$(document).ready(function() {

    $('#subscription-submit').on("click", function(){
      var email = $('#semail').val();
      if(validateEmail(email)){
        $("#semail").hide();
        $(".alert-success-modal-pre").hide().show('medium');
        $(".alert-success-modal-pre").fadeTo(1000, 100).slideUp(100, function(){
                $(".alert-success-modal-pre").slideUp(500);
                    });
        $('#subscription-form').submit();
      }else{
        $(".alert-error").hide().show('medium');
              $(".alert-error").fadeTo(2000, 100).slideUp(100, function(){
             // $(".alert-error").slideUp(500); 
            }); 
      }
      
    });
    $('#subscription-form').submit(function (event) {
      event.preventDefault();
          var email = $('#semail').val();
          var token = $('#_token').val();
          $.ajax({
                type: "POST",
                dataType : 'json',
                url: '/subscribe',
                data: {
                    _token: token,
                    email: email,
                },
                success: function( msg ) {
                  a = msg.success;
                  if(a == 'success'){

                    $(".alert-success-modal-pre").hide();
                    $(".alert-success-modal").show('medium');
                     $(".alert-success-modal").fadeTo(3000, 100).slideUp(100, function(){
                        $(".alert-success-modal").slideUp(500);
                        $("#modal-subscribe").modal('hide');
                        $("#semail").delay( 800 ).show();
                    }); 
                    //
                    console.log('walking');
                  }else{
                    $(".alert-success-modal-pre").hide();
                    $("#semail").show();
                    $(".alert-error").hide().show('medium');
                    $(".alert-error").fadeTo(2000, 100).slideUp(100, function(){
                    $(".alert-error").slideUp(500);
                    });
                  }
                },
                error: function( error ) {
                  console.log('not working');
                }
            });
          $("#semail").val('');
          });
        });
</script>
