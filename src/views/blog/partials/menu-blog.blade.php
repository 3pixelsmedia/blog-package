<nav class="navbar navbar-default navbar-static-top col-md-12" id="main-menu" @if(!Auth::guest() && Route::currentRouteName() == 'home')  @endif>
    <div class="container">
        <div class="navbar-header">
            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- Branding Image -->
            <a class="navbar-brand menu-opt" href="#" >
                <center class='logo-movil'>
                <strong style="color:white; font-size:20px;">Blog Logo</strong>
                <!-- <img class='img-responsive' src="" alt=""> -->
                    <!-- <div class='col-xs-10 col-xs-offset-1' >
                        
                        
                    </div> -->
                </center>
                <span class="sr-only">Blog</span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                    <li><a href="#" class="menu-opt">Link 1</a></li>
                    <li><a href="#" class="menu-opt">Link 2</a></li>
                    <li><a href="#" class="menu-opt">Link 3</a></li>
                    <li><a href="#" class="menu-opt" id="menu-contact">Link 4</a></li>
                     
                    
                     
                    <li><a href="#" class="btn btn-primary btn-subsciption "  id="btn-subscription" type="button" data-toggle="modal" data-target="#modal-subscribe" >Subscription</a></li>
                     
                    
            </ul>
        </div>
    </div>
</nav>
