@foreach($posts as $index => $row)
<!-- Blog Post -->
    <article class="col-md-4 card-post">
        <!-- Title -->
       
        <p class="img-post-list">
        <a href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>
                <img src="{{ !empty($row->preview_image)? Storage::url($row->preview_image) : NULL }}"/>
            </a>    
        </p>
        
        <span class="cat-span"><a href='{{URL::to("/")}}/blog/categorias/{{$row->slug_category }}'>{{ $row->name_categories }}</a></span>
        <h2 class="post-title"> 
            <a class="primary-color-text" href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>{{ $row->title }}</a>
        </h2>
            <!-- Author -->
        <p class="lead-blog">
           Por: {{ $row->name_author }}
        </p>
        <!-- Date/Time -->
        <p class="date-blog"><span class="glyphicon glyphicon-time"></span>{{ date('M,d Y',strtotime($row->created_at)) }}</p>
        <!-- Post Content -->
        <p class="post-resume">{{ $row->summary }}<p>
        <span class="read-more">
            <a href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>Leer Más</a>
        </span>
    </article>
    <!-- /Blog Post --> 
    @if(($index+1)%3 == 0)
        <div class="clear"></div>
    @endif
@endforeach