@extends('blog.layout')

    @section('title', $page_title)

@section('primary-content')
    @include('blog.partials.menu-blog')

    @include('blog.partials.carousel')

@endsection

@section('content')
    <div class="separator"></div>
    @php 
        $locale = App::getLocale();
    @endphp 

    {!! getBlogWidgets('LIST', 'top-section') !!}

    {!! getBlogWidgets('LIST', 'before-content') !!}

    @if(Route::currentRouteName() == 'blog.search')
        <div class="row">
            <div class="col-md-10 list-title">
                <span>Se han encontrado {{ count($result) }} resultados</span>
            </div>
        </div>
    @endif
    
    @foreach($result as $index => $row)
        <!-- Blog Post -->

        <article class="col-md-4 card-post">
            <!-- Title -->
            
            <p class="img-post-list">
                <a href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>
                    <img src='{{ !empty($row->preview_image)? Storage::url($row->preview_image) : NULL }}' style="max-width: 100%"/>
                </a>
            </p>
            
            
            <h2 class="post-title"> <a class="primary-color-text" href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>{{ $row->title }}</a></h2>
                <!-- Author -->
            <p class="lead-blog">
            Por: {{ $row->name_author }}
            </p>
            <!-- Date/Time -->
            <p class="date-blog"><span class="glyphicon glyphicon-time"></span>{{ date('M,d Y',strtotime($row->created_at)) }}</p>
            <!-- Post Content -->
            <p class="post-resume">{{ $row->summary }}<p>
            <span class="read-more"><a href='{{URL::to("/")}}/blog/{{ $row->slug }}' title='{{$row->title}}'>Leer Más</a></span>

        </article>
        <!-- /Blog Post --> 
    @endforeach


    @if(count($result)==0)
        <div class='alert alert-info'>Ningún artículo encontrado</div>
    @endif

    {!! getBlogWidgets('List', 'after-content') !!}

    {!! getBlogWidgets('List', 'bottom-section') !!}   

    <div class="col-xs-12 text-center" style="margin-top: 5%; margin-bottom: 5%;">
        <a class="btn btn-primary btn-form-send btn-form-send-blue" style="text-align: center !important; float:none;" href='{{URL::to("/")}}/blog/'>Regresar</a>
    </div>     

@endsection