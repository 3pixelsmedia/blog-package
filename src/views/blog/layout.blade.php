<!DOCTYPE html>
<html lang="es">

<head>
    

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

@if($interface == 'article')
    <meta name="description" content="{{ $row->summary }}" />
    <meta name="author" content="{{$author}}">
    <!-- Open Graph data -->
    <meta property="og:title" content="@yield('title')" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="{{ $row->url }}" />
    <meta property="og:image" content="{{ (!empty($row->rrss_image))? Storage::url($row->preview_image) : NULL }}" />
    <meta property="og:description" content="{{ str_limit(strip_tags($row->content),155) }}" />
    <!-- Twitter Card data -->
    <meta name="twitter:card" value="summary">
    <meta name="twitter:site" content="{{$twitter}}">
    <meta name="twitter:title" content="@yield('title') {{$blog_name}}">
    <meta name="twitter:description" content="{{ str_limit(strip_tags($row->content),155) }}">
    <meta name="twitter:creator" content="{{$author}}">
    <meta name="twitter:image:src" content="{{ (!empty($row->rrss_image))? Storage::url($row->preview_image) : NULL }}">
@endif
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,700" rel="stylesheet"> 
    <!--link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet"-->
    <link href="{{asset('blogtheme/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('blogtheme/css/blog-style.css')}}" rel="stylesheet">
    <!-- Fonts -->
    <!--------------------------------------------------
                         CUSTOM CSS 
    --------------------------------------------------->
    <link href="{{asset('blogtheme/css/blog-style-custom.css')}}" rel="stylesheet">
    <!--------------------------------------------------
                        / CUSTOM CSS 
    --------------------------------------------------->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> -->
    <script src="{{asset('blogtheme/js/bootstrap.min.js')}}"></script>
    <!-- <script src="/js/bootstrap.js"></script> --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.0.3/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.3/vue-resource.min.js"></script>


    

    



    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body >



        <div id="app" class="index">
            <div id="primary-content">
                @yield('primary-content')

                <!-- Blog Sidebar Widgets Column -->
                <div class="container">
                <div class="col-md-8 col-md-offset-2 offset-md-2 col-sm-10 col-sm-offset-1 offset-sm-1  col-xs-12  @if(Route::currentRouteName() == 'blog.search' || Route::currentRouteName() == 'blog.category') blog-search-form-container @else blog-search-form-container-other-pages @endif">

                            
                            <!-- Blog Search Well -->
                            <div class="well-blog hidden-xs d-none d-md-block">
                                
                                <form class="form-inline" method='get' action='{{ url("search") }}'>
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="input-group  col-md-9 col-sm-9 col-xs-8" style="float:left;">
                                    <input type="text" name="q" placeholder="{{$search}}:" @if(!empty($header_title) && $interface != 'category' ) value="{{ $header_title }}" @endif   class="form-control blog-search" id="blog-search-input">
                                    <span class="glyphicon glyphicon-search search-icon form-control-feedback"></span>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-4">
                                    <button class="btn btn-primary btn-form-send btn-form-send-blue" id="btn-search-blog">{{$search}}</button>
                                </div>
                                
                                </form>
                                <!-- /.input-group -->
                            </div>
                            
                        </div>
                
                
                </div>
                        
                    <!-- Blog Sidebar Widgets Column -->
                    <div class="clear"></div>
            </div>
            <div class="content">
                <!-- Page Content -->
                <div class="container">

                    <div class="row">

                        

                        <!-- Blog Post Content Column -->
                        <div class="col-md-12 post-container" >

                            @yield('content')                

                        </div>
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            
            @include('blog.partials.modal-subscribe')
            <div style="height:100px"></div>
            <br class="clear">
            <div class="index">
            @include('blog.partials.footer')
            </div>
            
        </div>


   
    <!-- jQuery -->
    <!-- <script src="{{asset('blogtheme/js/jquery.js')}}"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <!-- <script src="{{asset('blogtheme/js/bootstrap.min.js')}}"></script> -->
   <script>
       if($("#btn-more-articles").length > 0)
        {
            $("#btn-more-articles").click(function(e){
                e.preventDefault();
                getMoreArticles();
            })
        }
       function getMoreArticles()
        {
            var articles = $(".card-post").length;

            $.ajax({
                method: "POST",
                data: {'articles':articles},
                url: "/blog-get-more-posts",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function( data ) {
                $("#posts-list-container").append(data.html);
                
                if(!data.btn_more_show){
                    $("#btn-more-articles").hide();
                    $('#btn-more-articles').css({"visibility":"hidden"});
                }
                    

            }).fail(function(dataError) {
                $('#loading').modal('hide');
            });	
        }
   </script>

</body>

</html>
