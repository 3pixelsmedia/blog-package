<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categories_id')->nullable();
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('lang')->nullable();
            $table->longtext('content')->nullable();
            $table->string('summary',350)->nullable();
            $table->string('main_image')->nullable();
            $table->string('preview_image')->nullable();
            $table->string('rrss_image')->nullable();
            $table->string('url')->nullable();
            $table->string('tags')->nullable();
            $table->integer('cms_users_id')->nullable();
            $table->boolean("is_draft")->default(false);
            $table->boolean("active")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_posts');
    }
}
