<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogWidgets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_widgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('blog_post_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('blog_page');
            $table->string('locale')->nullable();
            $table->integer('order');
            $table->string('location');
            $table->text('html');
            $table->string('vars_replace')->nullable();
            $table->boolean("active")->default(true);
            $table->timestamps();
        });

        Schema::table('blog_widgets', function (Blueprint $table) {
            $table->foreign('blog_post_id')->references('id')->on('blog_posts')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_widgets', function (Blueprint $table) {
            $table->dropForeign('blog_widgets_blog_post_id_foreign');
            $table->dropIndex('blog_widgets_blog_post_id_foreign');
        });

        Schema::dropIfExists('blog_widgets');
    }
}
