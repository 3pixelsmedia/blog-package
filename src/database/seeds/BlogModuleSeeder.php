<?php

use Illuminate\Database\Seeder;

class BlogModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Please wait updating the data...');                
        $this->call('BlogModulesData');
        $this->call('WidgetsModulesData');         
        $this->command->info('Updating the data completed !');
    }
}
class BlogModulesData extends Seeder {
    public function run() {        
    	
    	DB::table('cms_moduls')->insert(
    		[11=>['id'=>12,'created_at'=>'2018-11-21 15:21:17','updated_at'=>NULL,'name'=>'Blog Categories','icon'=>'fa fa-tags','path'=>'blog-categories','table_name'=>'blog_categories','controller'=>'AdminBlogCategoriesController','is_protected'=>0,'is_active'=>0],
            12=>['id'=>13,'created_at'=>'2018-11-21 15:22:41','updated_at'=>NULL,'name'=>'Blog Posts','icon'=>'fa fa-file-text-o','path'=>'blog-posts','table_name'=>'blog_posts','controller'=>'AdminBlogPostsController','is_protected'=>0,'is_active'=>0],
            13=>['id'=>14,'created_at'=>'2018-11-21 15:21:18','updated_at'=>NULL,'name'=>'Blog Widgets','icon'=>'fa fa-code','path'=>'blog-widgets','table_name'=>'blog_widgets','controller'=>'AdminBlogWidgetsController','is_protected'=>0,'is_active'=>0]]
    	);

        DB::table('cms_menus')->insert([0=>['id'=>1,'name'=>'Blog Categories','type'=>'Route','path'=>'AdminBlogCategoriesControllerGetIndex','color'=>NULL,'icon'=>'fa fa-tags','parent_id'=>0,'is_active'=>1,'is_dashboard'=>0,'id_cms_privileges'=>1,'sorting'=>1,'created_at'=>'2018-11-21 15:21:18','updated_at'=>NULL],
        1=>['id'=>2,'name'=>'Blog Posts','type'=>'Route','path'=>'AdminBlogPostsControllerGetIndex','color'=>NULL,'icon'=>'fa fa-file-text-o','parent_id'=>0,'is_active'=>1,'is_dashboard'=>0,'id_cms_privileges'=>1,'sorting'=>2,'created_at'=>'2018-11-21 15:22:41','updated_at'=>NULL],
        2=>['id'=>3,'name'=>'Blog Widgets','type'=>'Route','path'=>'AdminBlogWidgetsControllerGetIndex','color'=>NULL,'icon'=>'fa fa-code','parent_id'=>0,'is_active'=>1,'is_dashboard'=>0,'id_cms_privileges'=>1,'sorting'=>3,'created_at'=>'2018-11-21 15:22:41','updated_at'=>NULL]]);

        DB::table('cms_menus_privileges')->insert([
            0=>['id'=>1,'id_cms_menus'=>1,'id_cms_privileges'=>1],
            1=>['id'=>2,'id_cms_menus'=>2,'id_cms_privileges'=>1],
            2=>['id'=>3,'id_cms_menus'=>3,'id_cms_privileges'=>1],
        ]);
    }
}
class WidgetsModulesData extends Seeder {
    public function run() {   
        DB::table('blog_widgets')->insert(
            [0=>['id'=>1,
                 'blog_post_id'=>NULL,
                 'name'=>'social-widget',
                 'description'=>'Widget para compartir en redes sociales',
                 'blog_page'=>'Detail',
                 'locale'=>NULL,
                 'order'=>'1',
                 'location'=>'social-section',
                 'html'=>'<div class="social-blog"><ul><li><a id="fb-sharer" href="" target="_blank"><img src="https://smartstemplus.s3.amazonaws.com/icons/social-fb-icon.png" class="social-icon" alt="facebook-icon"></a><li><li><a id="tw-sharer" href="" target="_blank"><img src="https://smartstemplus.s3.amazonaws.com/icons/social-tw-icon.png" class="social-icon" alt="twitter-icon"></a><li><li><a id="ln-sharer" href="" target="_blank"><img src="https://smartstemplus.s3.amazonaws.com/icons/social-ln-icon.png" class="social-icon" alt="linkedin-icon"></a><li></ul></div>',
                 'vars_replace'=>NULL,
                 'created_at'=>'2018-11-21 15:21:17',
                 'updated_at'=>NULL,
            ]
            ]);
    }
}