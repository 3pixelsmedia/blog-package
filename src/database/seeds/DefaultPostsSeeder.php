<?php

use Illuminate\Database\Seeder;

class DefaultPostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog_posts')->insert([
            0=>[
                'id'=>1,
                'created_at'=>'2017-03-22 15:26:08',
                'updated_at'=>'2017-03-22 15:31:09',
                'categories_id'=>1,
                'title'=>'Lorem ipsum dolor 1',
                'slug'=>'lorem-ipsum-dolor-sit-amet',
                'summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor', 
                'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'tags'=>'',
                'lang'=>'es',
                'main_image'=>'images/placeholder_main_img.png',
                'preview_image'=>'images/placeholder_preview_img.png',
                'rrss_image'=>'images/placeholder_rrss_img.png',
                'url'=>'',
                'cms_users_id'=>1,
                'is_draft'=>0,
                'active'=>1
                ],
           1=>[
                'id'=>2,
                'created_at'=>'2017-03-22 15:26:08',
                'updated_at'=>'2017-03-22 15:31:09',
                'categories_id'=>1,
                'title'=>'Lorem ipsum dolor 2',
                'slug'=>'lorem-ipsum-dolor-sit-amet-2',
                'summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor', 
                'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'tags'=>'',
                'lang'=>'es',
                'main_image'=>'images/placeholder_main_img.png',
                'preview_image'=>'images/placeholder_preview_img.png',
                'rrss_image'=>'images/placeholder_rrss_img.png',
                'url'=>'',
                'cms_users_id'=>1,
                'is_draft'=>0,
                'active'=>1
            ],
            2=>[
                'id'=>3,
                'created_at'=>'2017-03-22 15:26:08',
                'updated_at'=>'2017-03-22 15:31:09',
                'categories_id'=>1,
                'title'=>'Lorem ipsum dolor 3',
                'slug'=>'lorem-ipsum-dolor-sit-amet-3',
                'summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor', 
                'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'tags'=>'',
                'lang'=>'es',
                'main_image'=>'images/placeholder_main_img.png',
                'preview_image'=>'images/placeholder_preview_img.png',
                'rrss_image'=>'images/placeholder_rrss_img.png',
                'url'=>'',
                'cms_users_id'=>1,
                'is_draft'=>0,
                'active'=>1],
             3=>[
                'id'=>4,
                'created_at'=>'2017-03-22 15:26:08',
                'updated_at'=>'2017-03-22 15:31:09',
                'categories_id'=>2,
                'title'=>'Lorem ipsum dolor 4',
                'slug'=>'lorem-ipsum-dolor-sit-amet-4',
                'summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor', 
                'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'tags'=>'',
                'lang'=>'es',
                'main_image'=>'images/placeholder_main_img.png',
                'preview_image'=>'images/placeholder_preview_img.png',
                'rrss_image'=>'images/placeholder_rrss_img.png',
                'url'=>'',
                'cms_users_id'=>1,
                'is_draft'=>0,
                'active'=>1],
             4=>[
                'id'=>5,
                'created_at'=>'2017-03-22 15:26:08',
                'updated_at'=>'2017-03-22 15:31:09',
                'categories_id'=>2,
                'title'=>'Lorem ipsum dolor 5',
                'slug'=>'lorem-ipsum-dolor-sit-amet-5',
                'summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor', 
                'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'tags'=>'',
                'lang'=>'es',
                'main_image'=>'images/placeholder_main_img.png',
                'preview_image'=>'images/placeholder_preview_img.png',
                'rrss_image'=>'images/placeholder_rrss_img.png',
                'url'=>'',
                'cms_users_id'=>1,
                'is_draft'=>0,
                'active'=>1],
            5=>[
                'id'=>6,
                'created_at'=>'2017-03-22 15:26:08',
                'updated_at'=>'2017-03-22 15:31:09',
                'categories_id'=>2,
                'title'=>'Lorem ipsum dolor 6',
                'slug'=>'lorem-ipsum-dolor-sit-amet-6',
                'summary'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor', 
                'content'=>'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',
                'tags'=>'',
                'lang'=>'es',
                'main_image'=>'images/placeholder_main_img.png',
                'preview_image'=>'images/placeholder_preview_img.png',
                'rrss_image'=>'images/placeholder_rrss_img.png',
                'url'=>'',
                'cms_users_id'=>1,
                'is_draft'=>0,
                'active'=>1],
                ]);
    }
}
