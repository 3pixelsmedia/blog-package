<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog_categories')->insert([  
            0=>[  
                'id'            =>1,        
                'created_at'    =>date('Y-m-d H:i:s'),
                'created_at'    =>date('Y-m-d H:i:s'),
                'name'          =>'Prensa',
                'slug'          =>'prensa'],
            1=>[  
                'id'            =>2,        
                'created_at'    =>date('Y-m-d H:i:s'),
                'created_at'    =>date('Y-m-d H:i:s'),
                'name'          =>'Seguros',
                'slug'          =>'seguros'],
            
            ]);
    }
}
