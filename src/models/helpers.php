<?php 

use App\BlogWidget;


function getBlogWidgets($blogPage, $location, $post = NULL)
{
    try
    {	
    	$widgets = BlogWidget::where('blog_page','=',$blogPage)->where('location','=',$location)->orderBy('order','asc')->get();
        //echo $widgets;
    	$widgets = replaceVarsInWidget($widgets, $post);

    	$html = view('blog.widget', compact('widgets'))->render();

        return $html;
        
    } catch (Exception $e) {
        Log::info('Exception generada por la función getBlogWidgets del helper');
    }
}


function replaceVarsInWidget($widgets, $post = NULL)
{
	if(!empty($post))
	{
		$patterns = array(
			'[CREATED_AT]',
			'[IMG_BIG]',
			'[IMG_FACEBOOK]',
			'[IMG_SUMMARY]',
			'[LANG]',
			'[SLUG]',
			'[SUMMARY]',
			'[TAGS]',
			'[TITLE]',
			'[URL]',
			'[UPDATED_AT]',
		);

		$replaceValues = array(
			\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->created_at)->format('Y-m-d'),
			$post->img_big,
			$post->img_facebook,
			$post->img_summary,
			$post->lang,
			$post->slug,
			$post->summary,
			$post->tags,
			$post->title,
			$post->url,
			!empty($post->updated_at)? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $post->updated_at)->format('Y-m-d') : NULL
		);
	}
	else
	{
		$patterns = array(
			'[URL]',	
		);

		$replaceValues = array(
			url()->full()
		);
	}

	foreach ($widgets as $widget) 
	{
		$widgetString = $widget->html;

		$widgetString = str_replace($patterns, $replaceValues, $widgetString);
		
	    $widget->html = $widgetString;
	}

	return $widgets;
}