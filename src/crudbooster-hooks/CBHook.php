<?php 
namespace App\Http\Controllers;

use DB;
use Session;
use Request;

class CBHook extends Controller {

	/*
	| --------------------------------------
	| Please note that you should re-login to see the session work
	| --------------------------------------
	|
	*/
	public function afterLogin() {
		$users 		= DB::table('cms_users')->where("id",Session::get('admin_id'))->first(); 	
		$photo = ($users->photo)? $users->photo:asset('vendor/crudbooster/avatar.jpg');
		Session::put('admin_photo',$photo);
	}
}