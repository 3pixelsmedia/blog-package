<?php

namespace Trespixelsmedia\Blog;

use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->publishes([
            __DIR__.'/views/blog/' => resource_path('views/blog'),
            __DIR__.'/database/migrations/' => database_path('migrations'),
            __DIR__.'/database/seeds/' => database_path('seeds/blog'),
            __DIR__.'/Controllers/' => app_path('Http/Controllers'),
            __DIR__.'/Traits/' => app_path('Traits'),
            __DIR__.'/models/' => app_path(),
        ], 'blog');

        $this->publishes([
            __DIR__.'/assets/blogtheme/' => public_path('blogtheme'),
        ], 'blogtheme');

        $this->publishes([
            __DIR__.'/views/crudbooster/' => resource_path('views/vendor/crudbooster/'),
            __DIR__.'/crudbooster-hooks/' => app_path('Http/Controllers'),
        ], 'crudbooster-storage');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
