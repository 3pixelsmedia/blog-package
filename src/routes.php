<?php

//**** Blog (Public Routes) *****************************************************
		//Blog Routes Blog Home
		Route::get('blog','FrontBlogController@getIndex');

		//Blog Routes Search
		Route::get('search','FrontBlogController@getSearch');

		//Blog Route Latest
		Route::get('latest','FrontBlogController@getLatest');

		//Blog Route Category
		//Route::get('category/{id}/{slug}','FrontBlogController@getCategory');
		Route::get('blog/categorias/{slug}','FrontBlogController@getCategory');

		//Blog Route Article
		Route::get('blog/{slug}','FrontBlogController@getArticle');
		//Route::post("/subscribe", "SubscribeController@save");
		Route::post('subscribe','SubscribeController@save')->name('subscribe-form');

		//Get more articles (Blog)
		Route::post('/blog-get-more-posts','FrontBlogController@getMorePosts');
//*****************************************************************************