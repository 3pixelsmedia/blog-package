<?php 
namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Request;

use CB;

trait CrudBoosterTrait
{
	public function postUploadSummernote() 
	{		
		$this->cbLoader();
		$name = 'userfile';
		$uploadTypes = explode(',',config('crudbooster.UPLOAD_TYPES'));
		$uploadMaxSize = config('crudbooster.DEFEAULT_UPLOAD_MAX_SIZE')?:5000;
		if (Request::hasFile($name))
		{
			$file = Request::file($name);
			//$ext  = $file->getClientOriginalExtension(); 
			$ext  = $file->guessExtension();
			$filesize = $_FILES[$name]['size'] / 1000;
			$filePath = 'uploads/'.CB::myId().'/'.date('Y-m');

			if($filesize <= $uploadMaxSize) {
				if(in_array($ext, $uploadTypes)) {
					//Create Directory Monthly
					Storage::makeDirectory($filePath);
					//Move file to storage
					$filename = md5(str_random(5)).'.'.$ext;					
					Storage::putFileAs($filePath,$file,$filename);
					echo Storage::url($filePath.'/'.$filename);
					// echo json_encode(array('location'=>Storage::url($filePath.'/'.$filename)));
				}else{
					echo "http://placehold.it/250x250&text=File+Not+Allowed";
				}
			}else{
				echo "http://placehold.it/250x250&text=File+Too+Large";			
			}					
		}
	}
}