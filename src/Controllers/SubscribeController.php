<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\Subscribe;
use Mailchimp;
use Mailchimp_Error;
use Mailchimp_List_AlreadySubscribed;

class SubscribeController extends Controller
{
	public $mailchimp;
    //public $listId = 'db69f14490';
    //public $listId = '48c9b055f2';
    public $listId = '0b88291ea8';
    
    

    public function __construct(\Mailchimp $mailchimp)
    {
        $this->mailchimp = $mailchimp;
    }

     public function save(Request $request){

        //dd($request->email);
        $semail =  $request->email;
        if (filter_var($semail, FILTER_VALIDATE_EMAIL)) {
          
            \DB::table('subscribers')
                    ->insert([
                        'email' => $request->email
                    ]);
        
            //\Mail::to($request->email)->send(new Subscribe($request->email));

            try {

                $this->mailchimp
                ->lists
                ->subscribe(
                    $this->listId,
                    ['email' => $request->email]
                );
                //return redirect('/');    
                return response()->json(['success' => 'success']); 

            } catch (\Mailchimp_List_AlreadySubscribed $e) {
                //dd($e);

            } catch (\Mailchimp_Error $e) {
                //dd($e);

            }

        //return redirect('/');
            return response()->json(['success' => 'success']); 

            } else {

            return response()->json(['success' => 'error']); 

        }
     }
}
