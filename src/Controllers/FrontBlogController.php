<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use DB;

class FrontBlogController extends Controller
{
    //data Variables, inser your custom data.
	public $blog_name = 'MyBlog';
	public $blog_description = 'Type here the blog description';
	public $twitter_site = '@SomeTwitter';
	public $search_label = 'Buscar';
	//End data variables
	
    public function getIndex() {

		//$numberPosts = Config::ofVar('number_posts_in_blog_homepage');
		//$locale = \LaravelLocalization::getCurrentLocale();
		$data['interface'] = 'blog-home';
		$data['page_title'] = 'Home | '.$this->blog_name;
    	$data['page_description'] = $this->blog_description;
		$data['blog_name'] = $this->blog_name;
		$data['search'] = $this->search_label;
    	$data['categories'] = DB::table('blog_categories')->get();
		
        //dd($data);

    	$data['result'] = DB::table('blog_posts')
    	->join('blog_categories','blog_categories.id','=','categories_id')
    	->join('cms_users','cms_users.id','=','cms_users_id')
    	->select('blog_posts.*','blog_categories.name as name_categories','blog_categories.slug as slug_category','cms_users.name as name_author')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->orderby('blog_posts.id','desc')
    	->take(6) //->take($numberPosts)
		->get();

		$data['allPosts'] = DB::table('blog_posts')
        ->join('blog_categories','blog_categories.id','=','categories_id')
        ->join('cms_users','cms_users.id','=','cms_users_id')
        ->select('blog_posts.*','blog_categories.name as name_categories','blog_categories.slug as slug_category','cms_users.name as name_author')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->orderby('blog_posts.id','desc')->count();
		
		//dd($data);
    	return view('blog.home',$data);
    }

    public function getArticle($slug) {
		//$numberPosts = Config::ofVar('number_posts_in_blog_detail');
		//$locale = \LaravelLocalization::getCurrentLocale();
    	$row = DB::table('blog_posts')
    	->join('blog_categories','blog_categories.id','=','categories_id')
    	->join('cms_users','cms_users.id','=','cms_users_id')
    	->select('blog_posts.*','blog_categories.name as name_categories','cms_users.name as name_author')
    	->where('blog_posts.slug',$slug)
		->first();
		$data['interface'] = 'article';
    	$data['row'] = $row;
		$data['page_title'] = $row->title.' | '.$this->blog_name;
		$data['author'] = $row->name_author;
		$data['twitter'] = $this->twitter_site;
		$data['url'] = $row->url;
    	$data['page_description'] = str_limit(strip_tags($row->content),155);
		$data['blog_name'] = $this->blog_name;
		$data['search'] = $this->search_label;
		$data['categories'] = DB::table('blog_categories')->get();
		
		$posts = DB::table('blog_posts')
    	->join('blog_categories','blog_categories.id','=','categories_id')
    	->join('cms_users','cms_users.id','=','cms_users_id')
    	->select('blog_posts.*','blog_categories.name as name_categories','blog_categories.slug as slug_category','cms_users.name as name_author')
        //->where('blog_posts.lang','=',$locale)
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->where('blog_posts.slug','<>',$slug)
    	->orderby('blog_posts.id','desc')
    	->take(3) //->take($numberPosts)
    	->get();
		$data['posts'] = $posts;

    	return view('blog.detail',$data);
    }

    public function getCategory($slug) {
    	//$row = DB::table('blog_categories')->where('id',$id)->first();
		//$numberPosts = Config::ofVar('number_posts_in_blog_category_paginate');
		//$locale = \LaravelLocalization::getCurrentLocale();
    	$row = DB::table('blog_categories')->where('slug',$slug)->first();

    	if(!$row) return redirect('/');

    	$data['result'] = DB::table('blog_posts')
    	->join('blog_categories','blog_categories.id','=','categories_id')
    	->join('cms_users','cms_users.id','=','cms_users_id')
    	->select('blog_posts.*','blog_categories.name as name_categories','cms_users.name as name_author')
    	->orderby('blog_posts.id','desc')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->where('blog_categories.slug',$slug)
    	->paginate(6);
		$data['interface'] = 'category';
    	$data['row'] = $row;
    	$data['page_title'] = $row->name.' | '.$this->blog_name;
    	$data['page_description'] = $data['page_title'];
    	$data['blog_name'] = $this->blog_name;
		$data['categories'] = DB::table('blog_categories')->get();
		$data['search'] = $this->search_label;
    	$data['header_title'] = $row->name;

    	return view('blog.list-category',$data);
    }

    public function getLatest() {    	
		//$locale = \LaravelLocalization::getCurrentLocale();
    	$data['result'] = DB::table('blog_posts')
    	->join('blog_categories','blog_categories.id','=','categories_id')
    	->join('cms_users','cms_users.id','=','cms_users_id')
    	->select('blog_posts.*','blog_categories.name as name_categories','cms_users.name as name_author')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->orderby('blog_posts.id','desc')    	
    	->paginate(6);
    	
    	$data['page_title'] = 'Latest | MySimpleBlog';
    	$data['page_description'] = $data['page_title'];
    	$data['blog_name'] = $this->blog_name;
    	$data['categories'] = DB::table('blog_categories')->get();
    	$data['header_title'] = 'Latest';

    	return view('blog.lists',$data);
    }

    public function getSearch(Request $req) {    	
		//$locale = \LaravelLocalization::getCurrentLocale();
    	if($req->get('q')=='') return redirect('/');

    	$data['result'] = DB::table('blog_posts')
    	->join('blog_categories','blog_categories.id','=','categories_id')
    	->join('cms_users','cms_users.id','=','cms_users_id')
    	->select('blog_posts.*','blog_categories.name as name_categories','blog_categories.slug as slug_category','cms_users.name as name_author')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		//->where('blog_posts.title','like','%'.$req->get('q').'%') 
		->where(function ($query) use ($req) {
            $query->where('blog_posts.title','like','%'.$req->get('q').'%')
                ->orWhere('blog_categories.name','like','%'.$req->get('q').'%')
                ->orWhere('blog_posts.content','like','%'.$req->get('q').'%');
        })	
    	->paginate(6);
    	
    	$data['page_title'] = $this->search_label.': '.$req->get('q').' | '.$this->blog_name;
    	$data['page_description'] = $data['page_title'];
    	$data['blog_name'] = $this->blog_name;
		$data['categories'] = DB::table('blog_categories')->get();
		$data['word_search'] = $req->get('q');
		$data['header_title'] = $this->search_label.': '.$req->get('q');
		$data['search'] = $this->search_label;
		$data['interface'] = 'blog-search';
    	return view('blog.lists',$data);
	}
	
	public function getMorePosts(Request $request)
    {
        $numberPosts = 6;
		//$locale = \LaravelLocalization::getCurrentLocale();
        $posts = DB::table('blog_posts')
        ->join('blog_categories','blog_categories.id','=','categories_id')
        ->join('cms_users','cms_users.id','=','cms_users_id')
        ->select('blog_posts.*','blog_categories.name as name_categories','blog_categories.slug as slug_category','cms_users.name as name_author')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->orderby('blog_posts.id','desc')
        ->offset($request->articles)
        ->limit($numberPosts)
        ->get();

        $morePosts = DB::table('blog_posts')
        ->join('blog_categories','blog_categories.id','=','categories_id')
        ->join('cms_users','cms_users.id','=','cms_users_id')
        ->select('blog_posts.*','blog_categories.name as name_categories','blog_categories.slug as slug_category','cms_users.name as name_author')
		->where([['blog_posts.is_draft','=',false],['blog_posts.active','=',true]])
		->orderby('blog_posts.id','desc')
        ->offset($request->articles + $posts->count())
        ->limit($numberPosts)
        ->get();

        $data['html'] = view('blog.posts-to-list', compact('posts'))->render();

        if($morePosts->count() > 0)
            $data['btn_more_show'] = true;
        else
            $data['btn_more_show'] = false;
       
        return $data;
	}
}
